/**
 * GLINT
 */
import '@glint/environment-ember-loose';
import '@glint/environment-ember-template-imports';

import type FieldComponentsRegistry from '@getflights/ember-field-components/template-registry';

declare module '@glint/environment-ember-loose/registry' {
  export default interface Registry
    extends FieldComponentsRegistry /* + other addon registries */ {
    // local entries
  }
}

/**
 * EMBER DATA
 */
// Catch-all for ember-data
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    [key: string]: unknown;
  }
}
