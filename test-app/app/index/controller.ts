import Controller from '@ember/controller';
import { tracked } from 'tracked-built-ins';
import { add, startOfDay } from 'date-fns';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';

export default class IndexController extends Controller {
  // client = new Client();

  @tracked disabled = false;

  toggleDisabled = () => {
    this.disabled = !this.disabled;
  };

  changeValue = (key: keyof this, newValue: unknown) => {
    // @ts-expect-error types
    this[key] = newValue;
    console.log('Value change', key, newValue);
  };

  @tracked someText?: string;

  @tracked someNumber?: number;
  // @ts-expect-error string is not a number
  @tracked someInvalidNumber?: number = 'Not a number';

  @tracked someRangedNumber?: number;
  // @ts-expect-error string is not a number
  @tracked someInvalidRangedNumber?: number = 'Not a number';

  @tracked minDate = startOfDay(new Date());

  incrementMinDate = () => {
    this.minDate = add(this.minDate, { days: 1 });
  };

  @tracked someDate?: Date;
  @tracked someDateWithDefault?: Date = new Date('2030-01-20');
  @tracked invalidDate = new Date(NaN);
  // @ts-expect-error string is not a date
  @tracked invalidDateString: Date = 'Invalid, string';

  @tracked someDatetime?: Date;

  @tracked someDatetimeWithDefault?: Date = (() => {
    const date = new Date('2030-01-20');
    date.setHours(12);
    date.setMinutes(34);
    date.setSeconds(56);
    return date;
  })();

  @tracked someDateRange?: [Date, Date] | undefined;
  @tracked someDateRangeWithDefault?: [Date, Date] = [
    new Date('2030-01-20'),
    new Date('2030-01-30'),
  ];
  @tracked someDateRangeWithInvalid: [Date, Date] = [
    new Date(NaN),
    // @ts-expect-error string is not a date
    'Invalid, string',
  ];

  @tracked someDatetimeRange?: [Date, Date] | undefined;
  @tracked someDatetimeRangeWithDefault?: [Date, Date] = (() => {
    const [date1, date2] = [new Date('2030-01-20'), new Date('2030-01-30')];
    date1.setHours(12);
    date1.setMinutes(34);
    date1.setSeconds(56);

    date2.setHours(12);
    date2.setMinutes(34);
    date2.setSeconds(56);
    return [date1, date2];
  })();

  @tracked someTime?: string;
  @tracked someTimeWithDefault?: string = '12:55:00';
  @tracked someTimeWithInvalid?: string = '12:55';
  @tracked someTimeWithInvalidString?: string = 'test';

  @tracked someBoolean?: boolean;
  @tracked someBooleanWithDefault?: boolean = true;
  // @ts-expect-error string is not a date
  @tracked someBooleanWithInvalid?: boolean = 'Not a boolean';

  @tracked someLongtext?: string;
  @tracked someLongtextWithDefault?: string = 'First line\nSecond line';

  get selectOptions(): (SelectOption | SelectOptionGroup)[] {
    return [
      {
        value: 'opt1',
        label: 'Option 1',
      },
      {
        value: 'opt2',
        label: 'Option 2',
      },
      {
        label: 'Group',
        selectOptions: [
          {
            value: 'opt3',
            label: 'Option 3',
          },
          {
            value: 'opt4',
            label: 'Option 4',
          },
        ],
      },
    ];
  }
  @tracked someSelected?: string;
  @tracked someSelectedWithDefault?: string = 'opt1';
  @tracked someSelectedWithInvalid?: string = 'Not an option';

  changeValueRequiredSelect = (key: keyof this, newValue: string) => {
    // @ts-expect-error types
    this[key] = newValue;
    console.log('Value change', key, newValue);
  };

  // multi select
  @tracked someSelecteds?: string[];
  @tracked someSelectedsWithDefault?: string[] = ['option1'];
  @tracked someSelectedsWithInvalid?: string[] = ['Not an option'];
}
