import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import OutputDateComponent from '@getflights/ember-field-components/components/output/date';
import { tracked } from 'tracked-built-ins';
import { setupIntl } from 'ember-intl/test-support';
import { hash } from '@ember/helper';

module('Integration | Component | output/date', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('Without time', async function (assert) {
    const state = tracked({
      value: undefined as Date | undefined,
    });

    await render(<template>
      <OutputDateComponent @value={{state.value}} data-test-output />
    </template>);

    assert.dom('[data-test-output]').hasText('', 'Value is empty at first.');
    assert
      .dom('[data-test-output]')
      .hasClass('output--empty', 'Output has class "output--empty".');

    // 1 MEI 2030
    state.value = new Date('2030-05-01');
    await settled();
    assert
      .dom('[data-test-output]')
      .hasText('2030-05-01', 'Value is "2030-05-01" after updating @value.');
    assert
      .dom('[data-test-output]')
      .doesNotHaveClass(
        'output--empty',
        'Output has does not have "output--empty".',
      );
  });

  test('With time', async function (assert) {
    const state = tracked({
      value: undefined as Date | undefined,
    });

    await render(<template>
      <OutputDateComponent
        @options={{hash displayTime=true}}
        @value={{state.value}}
        data-test-output
      />
    </template>);

    assert.dom('[data-test-output]').hasText('', 'Value is empty at first.');
    assert
      .dom('[data-test-output]')
      .hasClass('output--empty', 'Output has class "output--empty".');

    // 1 MEI 2030 12:34:56
    const date = new Date('2030-05-01');
    date.setHours(12);
    date.setMinutes(34);
    date.setSeconds(56);
    state.value = date;
    await settled();
    assert
      .dom('[data-test-output]')
      .hasText(
        '2030-05-01 12:34:56',
        'Value is "2030-05-01" after updating @value.',
      );
    assert
      .dom('[data-test-output]')
      .doesNotHaveClass(
        'output--empty',
        'Output has does not have "output--empty".',
      );
  });
});
