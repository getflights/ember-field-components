import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, settled } from '@ember/test-helpers';
import OutputDateRangeComponent from '@getflights/ember-field-components/components/output/date-range';
import { tracked } from 'tracked-built-ins';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | output/date-range', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('Output::DateRange', async function (assert) {
    const state = tracked({
      value: undefined as [Date, Date] | undefined,
    });

    await render(<template>
      <OutputDateRangeComponent @value={{state.value}} data-test-output />
    </template>);

    assert.dom('[data-test-output]').hasText('', 'Value is empty at first.');
    assert
      .dom('[data-test-output]')
      .hasClass('output--empty', 'Output has class "output--empty".');

    // 1 MEI 2030 + 10 NOVEMBER 2031
    state.value = [new Date('2030-05-01'), new Date('2031-11-10')];
    await settled();
    assert
      .dom('[data-test-output]')
      .hasText(
        '2030-05-01 \u2192 2031-11-10',
        'Value is "2030-05-01 \u2192 2031-11-10" after updating @value.',
      );
    assert
      .dom('[data-test-output]')
      .doesNotHaveClass(
        'output--empty',
        'Output has does not have "output--empty".',
      );
  });
});
