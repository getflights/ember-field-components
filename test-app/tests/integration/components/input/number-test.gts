import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { fillIn, render, settled } from '@ember/test-helpers';
import InputNumberComponent from '@getflights/ember-field-components/components/input/number';
import { tracked } from 'tracked-built-ins';
import { hash } from '@ember/helper';
import fireEvent from '@ember/test-helpers/dom/fire-event';
import { focus } from '@ember/test-helpers';
import { blur } from '@ember/test-helpers';

async function customTypeIn(
  selector: string,
  characters: string,
  options = { delay: 50 },
) {
  const element = document.querySelector(selector);

  if (!element) {
    throw new Error(
      `Element not found when calling \`customTypeIn('${selector}')\`.`,
    );
  }

  if (!('value' in element)) {
    throw new Error(
      `Element does not have a value attribute \`customTypeIn('${selector}')\`.`,
    );
  }

  await focus(selector);
  for (let i = 0; i < characters.length; i++) {
    element.value = characters.substring(0, i + 1);
    fireEvent(element, 'input', {
      data: characters[i],
    });
    await new Promise((resolve) => setTimeout(resolve, options.delay));
  }
  await blur(selector);
}

module('Integration | Component | input/number', function (hooks) {
  setupRenderingTest(hooks);

  test('Input::Number', async function (assert) {
    const state = tracked({
      value: undefined as number | undefined,
    });

    const valueChanged = (newValue: number | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputNumberComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');

    /**
     * 1. You can only enter numeric values (a.k.a no text values allowed)
     */
    await fillIn('[data-test-input]', 'Some text');

    assert
      .dom('[data-test-input]')
      .hasValue('', 'You cannot fill in text into a number input.');
    assert.strictEqual(state.value, undefined, 'Variable has not updated.');

    // 'e' stands for exponential and can technically be typed into a number input, but our input doesn't allow this.
    await fillIn('[data-test-input]', 'e');

    assert
      .dom('[data-test-input]')
      .hasValue('', 'You cannot fill in "e" into a number input.');
    assert.strictEqual(state.value, undefined, 'Variable has not updated.');

    // Text values being passed to @value are rendered, but cleared whenever you focus the element.
    // @ts-expect-error not a number
    state.value = 'Some text';
    await settled();

    assert
      .dom('[data-test-input]')
      .hasValue(
        'Some text',
        'Input has "some text" because that is what @value is set to.',
      );

    /**
     * For some reason focus() from test-helpers doesn't work reliably
     */
    // await focus('[data-test-input]');
    const input = document.querySelector('[data-test-input]') as
      | HTMLInputElement
      | undefined;
    input?.dispatchEvent(new Event('focus'));
    await settled();

    assert
      .dom('[data-test-input]')
      .hasValue(
        '',
        '[ONLY WORKS WHEN BROWSER IS NOT FOCUSSED] Input was cleared because text is cleared on a focus event.',
      );
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated to undefined by valueChanged.',
    );

    /**
     * 2. Inputting numbers works + they are correctly passed (as real numbers) to @valueChanged
     */
    await customTypeIn('[data-test-input]', '123.45');
    assert
      .dom('[data-test-input]')
      .hasValue('123.45', 'Value is "123.45" after filling in.');
    assert.strictEqual(
      state.value,
      123.45,
      'Variable has been updated by valueChanged.',
    );
    // It is actually a number
    assert.strictEqual(
      typeof state.value,
      'number',
      'Type of variable is a number, parsing ok.',
    );

    // Empty inputs return undefined
    await fillIn('[data-test-input]', '');
    assert
      .dom('[data-test-input]')
      .hasValue('', 'Value is "" after filling in nothing.');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated to undefined by valueChanged.',
    );

    // Negative numbers also work
    await customTypeIn('[data-test-input]', '-123.45');
    assert
      .dom('[data-test-input]')
      .hasValue('-123.45', 'Value is "-123.45" after filling in.');
    assert.strictEqual(
      state.value,
      -123.45,
      'Variable has been updated by valueChanged.',
    );

    // Changing @value results in the input being updated
    state.value = 678.9;
    await settled();
    assert
      .dom('[data-test-input]')
      .hasValue('678.9', 'Value is "678.90" after updating @value.');
  });

  test('Input::Number @precision and @decimals', async function (assert) {
    const state = tracked<{
      value: number | undefined;
      precision?: number;
      decimals?: number;
    }>({
      value: undefined as number | undefined,
    });

    const valueChanged = (newValue: number | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputNumberComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @options={{hash precision=state.precision decimals=state.decimals}}
        data-test-input
      />
    </template>);

    /**
     * Precision results in min and max values being set
     */
    // Precision 5, decimals undefined (0) --> -99999 to 99999
    state.precision = 5;
    await settled();
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'min',
        '-99999',
        'The input has the correct min property set by the precision argument.',
      );
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'max',
        '99999',
        'The input has the correct max property set by the precision argument.',
      );

    state.precision = 5;
    state.decimals = 2;
    await settled();

    // Precision 5, decimals 2 -> -999.99 to 999.99
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'min',
        '-999.99',
        'The input has the correct min property set by the precision (and decimals) argument.',
      );
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'max',
        '999.99',
        'The input has the correct max property set by the precision (and decimals) argument.',
      );

    // When there are also min and max values set by the user, the most strict value is being used.
    await render(<template>
      <InputNumberComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @options={{hash precision=state.precision decimals=state.decimals}}
        data-test-input
        min={{100}}
        max={{998}}
      />
    </template>);
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'min',
        '100',
        'The input has the correct min property (users argument stricter than precision).',
      );
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'max',
        '998',
        'The input has the correct max property (users argument stricter than the precision).',
      );

    await render(<template>
      <InputNumberComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @options={{hash precision=state.precision decimals=state.decimals}}
        data-test-input
        min={{-1000}}
        max={{1000}}
      />
    </template>);
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'min',
        '-999.99',
        'The input has the correct min property (precision stricter than users argument).',
      );
    assert
      .dom('[data-test-input]')
      .hasAttribute(
        'max',
        '999.99',
        'The input has the correct max property (precision stricter than users argument).',
      );

    // /**
    //  * Decimals results in a step attribute + does not allow more than the amount of decimals given as input
    //  */
    // state.precision = undefined;
    // state.decimals = 2;
    // await settled();

    // // Decimals 2 -> step 0.01, no more than 2 decimal numbers
    // assert
    //   .dom('[data-test-input]')
    //   .hasAttribute(
    //     'step',
    //     '0.01',
    //     'The input has the correct step property (by decimals argument).',
    //   );

    await fillIn('[data-test-input]', '0.456');

    assert
      .dom('[data-test-input]')
      .hasValue(
        '0.45',
        'Number with 3 decimals (0.456) is shortened to 2 decimals (0.45)',
      );

    /**
     * Doing 65.85 * 100 results in 65.849999999, which would result in 65.84 in the past.
     * That's fixed now, but I have added this to be sure.
     */
    await fillIn('[data-test-input]', '65.85');

    assert
      .dom('[data-test-input]')
      .hasValue(
        '65.85',
        '[Extra] Inputting 65.85 returns 65.85 and not 65.84 (see comment).',
      );
  });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputNumberComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
