import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render, settled } from '@ember/test-helpers';
import InputCheckboxComponent from '@getflights/ember-field-components/components/input/checkbox';
import { tracked } from 'tracked-built-ins';

module('Integration | Component | input/checkbox', function (hooks) {
  setupRenderingTest(hooks);

  test('Input::Checkbox', async function (assert) {
    const state = tracked({
      value: undefined as boolean | undefined,
    });

    const valueChanged = (newValue: boolean) => {
      state.value = newValue;
    };

    await render(<template>
      <InputCheckboxComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').isNotChecked('Is not checked at first.');

    await click('[data-test-input]');

    assert.dom('[data-test-input]').isChecked('Is checked after clicking.');
    assert.strictEqual(
      state.value,
      true,
      'Variable has been updated by valueChanged',
    );

    await click('[data-test-input]');

    assert
      .dom('[data-test-input]')
      .isNotChecked('Is not checked after clicking again.');
    assert.strictEqual(
      state.value,
      false,
      'Variable has been updated by valueChanged',
    );

    // @ts-expect-error string is not a boolean
    state.value = 'Not a boolean';
    await settled();

    assert
      .dom('[data-test-input]')
      .isChecked('Is checked when value is a non-boolean truthy value.');
  });

  test('@disabled', async function (assert) {
    const state = tracked({
      value: true as boolean | undefined,
    });

    const valueChanged = (newValue: boolean) => {
      state.value = newValue;
    };

    await render(<template>
      <InputCheckboxComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @disabled={{true}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').isChecked('Is checked.');
    assert.dom('[data-test-input]').isDisabled('Is disabled.');

    const inputElement = document.querySelector(
      '[data-test-input]',
    ) as HTMLInputElement;
    inputElement.click();
    await settled();
    // await click('[data-test-input]');

    assert
      .dom('[data-test-input]')
      .isChecked('Is still checked (because disabled).');

    assert.strictEqual(state.value, true, 'Variable is still true');

    // Try to force it
    inputElement.checked = false;
    inputElement.dispatchEvent(new Event('input'));

    assert
      .dom('[data-test-input]')
      .isNotChecked(
        'Input is not checked (user set the value by inspecting element)',
      );
    assert.strictEqual(
      state.value,
      true,
      'Variable is still true (valueChanged was not fired)',
    );
  });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputCheckboxComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
