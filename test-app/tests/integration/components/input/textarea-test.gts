import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { fillIn, render, settled } from '@ember/test-helpers';
import InputTextareaComponent from '@getflights/ember-field-components/components/input/textarea';
import { tracked } from 'tracked-built-ins';

module('Integration | Component | input/textarea', function (hooks) {
  setupRenderingTest(hooks);

  test('Input::Textarea', async function (assert) {
    const state = tracked({
      value: undefined as string | undefined,
    });

    const valueChanged = (newValue: string) => {
      state.value = newValue;
    };

    await render(<template>
      <InputTextareaComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');

    await fillIn('[data-test-input]', 'First line\nSecond line');

    assert
      .dom('[data-test-input]')
      .hasValue(
        'First line\nSecond line',
        'Value is "First line" and "Second line" after filling in.',
      );
    assert.strictEqual(
      state.value,
      'First line\nSecond line',
      'Variable has been updated by valueChanged',
    );

    state.value = 'Other text\nSecond line';
    await settled();
    assert
      .dom('[data-test-input]')
      .hasValue(
        'Other text\nSecond line',
        'Value is "Other text" and "Second line" after updating @value.',
      );
  });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputTextareaComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
