import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { render } from '@ember/test-helpers';
import { tracked } from 'tracked-built-ins';
import Component from '@glimmer/component';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import field from '@getflights/ember-field-components/decorators/field';
import InputFieldCheckboxComponent from '@getflights/ember-field-components/components/input-field/checkbox';
import type {
  InputFieldArguments,
  BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import InputFieldComponent from '@getflights/ember-field-components/components/input-field';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import type { TOC } from '@ember/component/template-only';
import type { InputFieldLayoutSignature } from '@getflights/ember-field-components/components/input-field/-layouts/index';

module('Integration | Component | input-field', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  /**
   * - Renders an InputFieldText by default
   * - Field's label is the one from FieldInformationService's getTranslatedFieldLabel
   */
  test('What InputField to choose? Defaults to text.', async function (assert) {
    const model = tracked({
      someProperty: 'ABC',
    });

    await render(<template>
      <InputFieldComponent @model={{model}} @field='someProperty' />
    </template>);

    assert
      .dom('input')
      .hasClass('input-text', 'Has input text, because no config was defined');

    const test = <
      O extends SomeModel,
      F extends FieldOf<O>,
    >(): FieldDecoratorOptions<O, F> => {
      return {
        type: 'test',
        inputField: InputFieldCheckboxComponent,
        inputFieldOptions: {},
      };
    };

    class ModelClass {
      @field(test())
      @tracked
      someProperty = new Date();
    }

    const model2 = new ModelClass();

    await render(<template>
      <InputFieldComponent @model={{model2}} @field='someProperty' />
    </template>);

    assert
      .dom('input')
      .hasClass(
        'input-checkbox',
        'Has input of type "checkbox", because a config was passed which specified InputFieldCheckbox',
      );
  });

  /**
   * Passes all arguments to the InputField::*
   */
  test('Arguments correctly passed to an InputField::*', async function (assert) {
    let component: TestInputField | undefined;

    class TestInputField extends Component<{
      Args: InputFieldArguments<any, any>;
      Element: any;
    }> {
      constructor(owner: any, args: any) {
        super(owner, args);
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        component = this;
      }
    }

    const test = <O extends SomeModel, F extends FieldOf<O>>(
      options?: BaseInputFieldOptions,
    ): FieldDecoratorOptions<O, F> => {
      return {
        type: 'test',
        inputField: TestInputField,
        inputFieldOptions: options ?? {},
      };
    };

    class ModelClass {
      @field(test())
      @tracked
      someProperty = 'ABC';

      @field(test())
      @tracked
      someOtherProperty = 'DEF';
    }

    const model = new ModelClass();
    const modelField: keyof ModelClass = 'someProperty';

    const expectedArgs = tracked<any>({});

    await render(<template>
      <InputFieldComponent
        @model={{model}}
        @field={{modelField}}
        @preSetHook={{expectedArgs.preSetHook}}
        @inline={{expectedArgs.inline}}
        @inputId={{expectedArgs.inputId}}
        @label={{expectedArgs.label}}
        @showErrors={{expectedArgs.showErrors}}
        @layout={{expectedArgs.layout}}
      />
    </template>);

    assert.strictEqual(component?.args.model, model, '@model');
    assert.strictEqual(component?.args.field, modelField, '@field');

    /**
     * Options that are passed as-is
     */
    expectedArgs.preSetHook = (some: string) => some + '-changed';
    expectedArgs.inline = true;
    expectedArgs.inputId = 'inputId';
    expectedArgs.label = 'label';
    expectedArgs.showErrors = true;

    const TestLayout: TOC<InputFieldLayoutSignature> = <template>
      <div class='layout'>
        {{#if @label}}
          <label for='id' class='label'>
            {{@label}}
          </label>
        {{/if}}

        {{#each @prefixes as |prefix|}}
          <div class='prefix'>
            {{prefix}}
          </div>
        {{/each}}

        <@inputComponent
          class='class{{if @errors " is-invalid"}}'
          id='id'
          ...attributes
        />

        {{#each @suffixes as |suffix|}}
          <div class='suffix'>
            {{suffix}}
          </div>
        {{/each}}

        {{#if @helptext}}
          <div class='helptext'>
            {{@helptext}}
          </div>
        {{/if}}

        {{#each @errors as |error|}}
          <div class='error'>
            {{error}}
          </div>
        {{/each}}
      </div>
    </template>;

    expectedArgs.layout = TestLayout;

    assert.strictEqual(
      component?.args.preSetHook,
      expectedArgs.preSetHook,
      '@preSetHook',
    );
    assert.strictEqual(component?.args.inline, expectedArgs.inline, '@inline');
    assert.strictEqual(
      component?.args.inputId,
      expectedArgs.inputId,
      '@inputId',
    );
    assert.strictEqual(component?.args.label, expectedArgs.label, '@label');
    assert.strictEqual(
      component?.args.showErrors,
      expectedArgs.showErrors,
      '@suffix',
    );
    assert.strictEqual(component?.args.layout, expectedArgs.layout, '@layout');

    /**
     * Arguments that are transformed / updated with more options
     */
    const inputFieldOptions: BaseInputFieldOptions = {
      readonly: true,
      required: true,
      inputOptions: {
        fieldOption: 'fieldOption',
        overwritten: 'will be overwritten',
      },
      prefix: 'fieldPrefix',
      suffix: 'fieldSuffix',
    };

    class ModelClass2 {
      @field(test(inputFieldOptions))
      @tracked
      someOtherProperty = 'DEF';
    }

    const model2 = new ModelClass2();

    const args2 = tracked<InputFieldArguments<ModelClass2, keyof ModelClass2>>({
      model: model2,
      field: 'someOtherProperty',
    });

    await render(<template>
      <InputFieldComponent
        @model={{args2.model}}
        @field={{args2.field}}
        @disabled={{args2.disabled}}
        @required={{args2.required}}
        @inputOptions={{args2.inputOptions}}
        @prefix={{args2.prefix}}
        @suffix={{args2.suffix}}
      />
    </template>);

    // if @disabled is set, use this, otherwise use inputFieldOptions.readonly
    assert.strictEqual(
      component?.args.disabled,
      true,
      'disabled is true (inputFieldOptions.readonly = true, @disabled = undefined)',
    );
    args2.disabled = false;
    assert.strictEqual(
      component?.args.disabled,
      false,
      'disabled is false (inputFieldOptions.readonly = true, @disabled = false)',
    );

    // if @required is set, use this, otherwise use inputFieldOptions.required
    assert.strictEqual(
      component?.args.required,
      true,
      'required is true (inputFieldOptions.required = true, @disabled = undefined)',
    );
    args2.required = false;
    assert.strictEqual(
      component?.args.required,
      false,
      'required is false (inputFieldOptions.required = true, @disabled = false)',
    );

    // inputOptions are combined (inputFieldOptions.inputOptions, followed by @inputOptions)
    assert.propEqual(
      component?.args.inputOptions,
      inputFieldOptions.inputOptions,
      'inputOptions comes from inputFieldOptions',
    );
    args2.inputOptions = {
      overwritten: 'was overwritten',
      argsProperty: 'argsProperty',
    };
    assert.propEqual(
      component?.args.inputOptions,
      {
        ...inputFieldOptions.inputOptions,
        ...args2.inputOptions,
      },
      'inputOptions comes from inputFieldOptions, merged with @inputOptions',
    );

    // prefixes are combined (input field options prefix(es) last - closest to the input)
    assert.propEqual(
      component?.args.prefix,
      [inputFieldOptions.prefix],
      'prefixes come from inputFieldOptions',
    );
    args2.prefix = 'prefix';
    assert.propEqual(
      component?.args.prefix,
      [args2.prefix, inputFieldOptions.prefix],
      'prefixes come from inputFieldOptions and @prefix (input field options prefix(es) last - closest to the input)',
    );

    // suffixes are combined (input field options prefix(es) first - closest to the input)
    assert.propEqual(
      component?.args.suffix,
      [inputFieldOptions.suffix],
      'suffixes come from inputFieldOptions',
    );
    args2.suffix = ['suffix', 'secondSuffix'];
    assert.propEqual(
      component?.args.suffix,
      [inputFieldOptions.suffix, ...args2.suffix],
      'suffixes come from inputFieldOptions and @suffix (input field options suffix(es) first - closest to the input)',
    );
  });
});
