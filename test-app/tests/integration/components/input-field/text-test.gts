import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { render } from '@ember/test-helpers';
import InputFieldTextComponent from '@getflights/ember-field-components/components/input-field/text';

module('Integration | Component | input-field/text', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('renders an input/text', async function (assert) {
    const model = { field: 'value' };

    await render(<template>
      <InputFieldTextComponent @model={{model}} @field={{'field'}} />
    </template>);

    assert.dom('input').hasClass('input-text', 'Input has "input-text" class.');
  });
});
