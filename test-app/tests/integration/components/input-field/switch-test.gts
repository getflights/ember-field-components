import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { render } from '@ember/test-helpers';
import InputFieldSwitchComponent from '@getflights/ember-field-components/components/input-field/switch';

module('Integration | Component | input-field/switch', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('renders an input/switch', async function (assert) {
    const model = { field: 'value' };

    await render(<template>
      <InputFieldSwitchComponent @model={{model}} @field={{'field'}} />
    </template>);

    assert
      .dom('input')
      .hasClass('input-checkbox', 'Input has "input-checkbox" class.');

    // has different 'fieldType', but how can we test this?
  });
});
