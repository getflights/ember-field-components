import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { render } from '@ember/test-helpers';
import InputFieldCheckboxComponent from '@getflights/ember-field-components/components/input-field/checkbox';

module('Integration | Component | input-field/checkbox', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('renders an input/checkbox', async function (assert) {
    const model = { field: 'value' };

    await render(<template>
      <InputFieldCheckboxComponent @model={{model}} @field={{'field'}} />
    </template>);

    assert
      .dom('input')
      .hasClass('input-checkbox', 'Input has "input-checkbox" class.');
  });
});
