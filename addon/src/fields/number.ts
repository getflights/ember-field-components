import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldNumberComponent from '../components/input-field/number.gts';
import type { InputNumberOptions } from '../components/input/number.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '../components/output-field/-base.gts';
import OutputFieldNumberComponent from '../components/output-field/number.gts';
import type { OutputNumberOptions } from '../components/output/number.gts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

export type FieldNumberOptions = BaseInputFieldOptions & {
  precision?: number;
  decimals?: number;
};

const number = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldNumberOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions: BaseInputFieldOptions<InputNumberOptions> = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions: BaseOutputFieldOptions<OutputNumberOptions> = {
    ...baseOutputFieldOptions,
  };

  // Precision & decimals
  if (options?.precision || options?.decimals) {
    if (!inputFieldOptions.inputOptions) {
      inputFieldOptions.inputOptions = {};
    }

    if (options.precision !== undefined) {
      inputFieldOptions.inputOptions!.precision = options.precision;

      if (!outputFieldOptions.outputOptions) {
        outputFieldOptions.outputOptions = {};
      }

      outputFieldOptions.outputOptions!.precision = options.precision;
    }

    if (options.decimals !== undefined) {
      inputFieldOptions.inputOptions!.decimals = options.decimals;

      if (!outputFieldOptions.outputOptions) {
        outputFieldOptions.outputOptions = {};
      }

      outputFieldOptions.outputOptions!.decimals = options.decimals;
    }
  }

  return {
    type: 'number',
    inputField: InputFieldNumberComponent,
    inputFieldOptions,
    outputField: OutputFieldNumberComponent,
    outputFieldOptions,
  };
};

export default number;
