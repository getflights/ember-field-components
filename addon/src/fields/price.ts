import InputFieldPriceComponent from '../components/input-field/price.gts';
import OutputFieldPriceComponent from '../components/output-field/price.gts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';
import number, { type FieldNumberOptions } from './number.ts';

type FieldPriceOptions = FieldNumberOptions;

const price = <O extends SomeModel, F extends FieldOf<O>>(
  options?: FieldPriceOptions,
): FieldDecoratorOptions<O, F> => {
  return {
    ...number(options),
    type: 'price',
    inputField: InputFieldPriceComponent,
    outputField: OutputFieldPriceComponent,
  };
};

export default price;
