import InputFieldPercentComponent from '../components/input-field/percent.gts';
import OutputFieldPercentComponent from '../components/output-field/percent.gts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';
import number, { type FieldNumberOptions } from './number.ts';

type FieldPercentOptions = FieldNumberOptions;

const percent = <O extends SomeModel, F extends FieldOf<O>>(
  options?: FieldPercentOptions,
): FieldDecoratorOptions<O, F> => {
  return {
    ...number(options),
    type: 'percent',
    inputField: InputFieldPercentComponent,
    outputField: OutputFieldPercentComponent,
  };
};

export default percent;
