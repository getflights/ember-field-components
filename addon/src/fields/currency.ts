import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldCurrencyComponent from '../components/input-field/currency.gts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

export type FieldCurrencyOptions = BaseInputFieldOptions;

const currency = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldCurrencyOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  return {
    type: 'currency',
    inputField: InputFieldCurrencyComponent,
    inputFieldOptions,
    // outputField: OutputFieldTextComponent,
    // outputFieldOptions,
  };
};

export default currency;
