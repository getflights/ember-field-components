import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldTextareaComponent from '../components/input-field/textarea.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '../components/output-field/-base.gts';
import OutputFieldPreComponent from '../components/output-field/pre.gts';
import type { OutputTextOptions } from '../components/output/text.ts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

type FieldTextareaOptions = BaseInputFieldOptions;

const textarea = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldTextareaOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions: BaseOutputFieldOptions<OutputTextOptions> = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions.outputOptions,
      multiLine: true,
    },
  };

  return {
    type: 'pre',
    inputField: InputFieldTextareaComponent,
    inputFieldOptions,
    outputField: OutputFieldPreComponent,
    outputFieldOptions,
  };
};

export default textarea;
