import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldEmailComponent from '../components/input-field/email.gts';
import type { FieldDecoratorOptions } from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

type FieldTextOptions = BaseInputFieldOptions;

const email = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldTextOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  return {
    type: 'email',
    inputField: InputFieldEmailComponent,
    inputFieldOptions,
    // outputField: OutputFieldTextComponent,
    // outputFieldOptions,
  };
};

export default email;
