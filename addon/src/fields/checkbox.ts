import type { FieldOf, SomeModel } from '..';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '../components/input-field/-base.gts';
import InputFieldCheckboxComponent from '../components/input-field/checkbox.gts';
import type { FieldDecoratorOptions } from '../decorators/field';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '../components/output-field/-base.gts';
import OutputFieldCheckboxComponent from '../components/output-field/checkbox.gts';

type FieldCheckboxOptions = BaseInputFieldOptions;

const checkbox = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldCheckboxOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'checkbox',
    inputField: InputFieldCheckboxComponent,
    inputFieldOptions,
    outputField: OutputFieldCheckboxComponent,
    outputFieldOptions,
  };
};

export default checkbox;
