import Component from '@glimmer/component';
import type EmberDataModel from '@ember-data/model';
import type { FieldOf, SomeModel } from '..';

interface FieldMessagesSignature<O extends SomeModel, F extends FieldOf<O>> {
  Element: HTMLDivElement;
  Args: {
    model: O;
    field: F;
  };
}

export const errorsFor = <O extends SomeModel, F extends FieldOf<O>>(
  model: O,
  field: F,
): string[] | undefined => {
  if ('errors' in model) {
    if (model['errors'] instanceof Map) {
      const errors = model['errors'].get(field);
      return !errors || Array.isArray(errors) ? errors : [errors];
    } else if ('errorsFor' in model['errors']) {
      // ED models
      return (model as unknown as EmberDataModel).errors.reduce(
        (errors, { attribute, message }) => {
          if (attribute === field) {
            errors.push(message);
          }
          return errors;
        },
        [],
      );
    }
  }

  return undefined;
};

export default class FieldMessagesComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<FieldMessagesSignature<O, F>> {
  get errors() {
    return errorsFor(this.args.model, this.args.field);
  }

  <template>
    {{#if this.errors}}
      <div class='field-messages' ...attributes>
        {{#each this.errors as |error|}}
          <small class='help-block'>
            {{error}}
          </small>
        {{/each}}
      </div>
    {{/if}}
  </template>
}
