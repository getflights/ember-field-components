import Component from '@glimmer/component';
import InputNumberComponent, {
  type InputNumberOptions,
} from '../input/number.gts';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class InputFieldPercentComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputNumberOptions>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    const options = {
      ...this.args,
    };

    if (options.suffix) {
      if (Array.isArray(options.suffix)) {
        options.suffix = ['%', ...options.suffix];
      } else {
        options.suffix = ['%', options.suffix];
      }
    } else {
      options.suffix = '%';
    }

    return options;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputNumberComponent}}
      @fieldType='percent'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
