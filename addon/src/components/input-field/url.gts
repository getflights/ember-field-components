import Component from '@glimmer/component';
import InputTextComponent from '../input/text.gts';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class InputFieldUrlComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputTextComponent}}
      @fieldType='url'
      {{! @glint-ignore }}
      ...attributes
      {{! Add type="email" }}
      type='url'
    />
  </template>
}
