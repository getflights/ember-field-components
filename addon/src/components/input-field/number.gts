import Component from '@glimmer/component';
import InputNumberComponent, {
  type InputNumberOptions,
} from '../input/number.gts';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class InputFieldNumberComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputNumberOptions>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputNumberComponent}}
      @fieldType='number'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
