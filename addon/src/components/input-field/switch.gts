import Component from '@glimmer/component';
import InputCheckboxComponent from '../input/checkbox.gts';
import InputFieldTemplate, { type InputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class InputFieldSwitchComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputCheckboxComponent}}
      @fieldType='switch'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
