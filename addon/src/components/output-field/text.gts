import Component from '@glimmer/component';
import OutputTextComponent, {
  type OutputTextOptions,
  type OutputTextSignature,
} from '../output/text.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldTextComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputTextOptions>;
  Element: OutputTextSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputTextComponent}}
      @fieldType='text'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
