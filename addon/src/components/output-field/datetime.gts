import Component from '@glimmer/component';
import OutputDateComponent, {
  type OutputDateOptions,
  type OutputDateSignature,
} from '../output/date.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';

export default class OutputFieldDatetimeComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputDateOptions>;
  Element: OutputDateSignature['Element'];
}> {
  get outputFieldArgs() {
    const args = { ...this.args };

    args.outputOptions = {
      ...args.outputOptions,
      // Display time to make it datetime
      displayTime: true,
    };

    return args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputDateComponent}}
      @fieldType='date'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
