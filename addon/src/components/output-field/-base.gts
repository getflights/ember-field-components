/** eslint-disable @typescript-eslint/no-explicit-any */
import Component from '@glimmer/component';
import type { ComponentLike } from '@glint/template';
import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information.ts';
import type FieldComponentsService from '../../services/field-components.ts';
import type { FieldOf, SomeModel } from '../../index.ts';
import {
  BaseFieldOptionsKeys,
  type BaseFieldOptions,
} from '../../decorators/field.ts';
import type { SomeOutputFieldLayout } from './-layouts/index.ts';
import type { SomeOutputArguments } from '../output/-base.ts';
import { get } from '@ember/object';

export const BaseOutputFieldOptionsKeys = [
  ...BaseFieldOptionsKeys,
  'outputOptions',
];

export const filterBaseOutputFieldOptions = <
  IO extends object = Record<never, never>,
>(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  options: Record<string, any>,
): BaseOutputFieldOptions<IO> => {
  return Object.entries(options).reduce(
    (filteredOptions, [key, value]) => {
      if (BaseOutputFieldOptionsKeys.includes(key)) {
        filteredOptions[key] = value;
      }

      return filteredOptions;
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    {} as Record<string, any>,
  );
};

/**
 * Options for the OutputField that can also be passed using the field() decorator
 */
export interface BaseOutputFieldOptions<
  IO extends object = Record<never, never>,
> extends BaseFieldOptions {
  /**
   * Options to be passed to the Output component.
   */
  outputOptions?: IO;
}

/**
 * Options for an OutputField* that can by passed as arguments
 */
export interface OutputFieldArguments<
  O extends SomeModel,
  F extends FieldOf<O>,
  IO extends object = Record<never, never>,
> extends BaseOutputFieldOptions<IO> {
  // Model / field / value
  /**
   * The model you want to render a field of.
   */
  model: O;
  /**
   * Key of the model's field you want to render.
   */
  field: F;
  /**
   * Inline: whether to render only the output
   */
  inline?: boolean;
  /**
   * Optional: override the value you want to render
   */
  overrideValue?: O[F];
  /**
   * Optional: overwrite the default layout lookup mechanism by passing your own.
   */
  layout?: SomeOutputFieldLayout;
  /**
   * Whether to show errors or not, defaults to true.
   * NOTE: Only works for Ember Data models
   */
  showErrors?: boolean;
}

export default class OutputFieldTemplate<
  O extends SomeModel,
  F extends FieldOf<O>,
  IO extends object,
  I extends ComponentLike<{
    Args: SomeOutputArguments<IO>;
    Element: HTMLElement;
  }>,
> extends Component<{
  Args: {
    outputFieldArgs: OutputFieldArguments<O, F, IO>;
    outputComponent: I;
    fieldType: string;
    layout?: SomeOutputFieldLayout;
  };
  Element: HTMLElement;
}> {
  @service declare fieldInformation: FieldInformationService;
  @service declare fieldComponents: FieldComponentsService;

  private get value() {
    if (this.args.outputFieldArgs.overrideValue !== undefined) {
      return this.args.outputFieldArgs.overrideValue;
    }

    const { model, field } = this.args.outputFieldArgs;
    return get(model, field);
  }

  private get layoutComponent() {
    return (
      this.args.layout ??
      this.args.outputFieldArgs.layout ??
      this.fieldComponents.outputFieldLayoutFor(this.args.fieldType)
    );
  }

  private get label(): string {
    const computedLabel = this.fieldInformation.getTranslatedFieldLabel(
      this.args.outputFieldArgs.model,
      this.args.outputFieldArgs.field,
    );
    return this.args.outputFieldArgs.label ?? computedLabel;
  }

  private get prefixes() {
    if (!this.args.outputFieldArgs.prefix) {
      return [];
    }

    return Array.isArray(this.args.outputFieldArgs.prefix)
      ? this.args.outputFieldArgs.prefix
      : [this.args.outputFieldArgs.prefix];
  }

  private get suffixes() {
    if (!this.args.outputFieldArgs.suffix) {
      return [];
    }

    return Array.isArray(this.args.outputFieldArgs.suffix)
      ? this.args.outputFieldArgs.suffix
      : [this.args.outputFieldArgs.suffix];
  }

  get outputOptions(): IO {
    // @ts-expect-error types of IO and this don't match
    return {
      ...this.args.outputFieldArgs.outputOptions,
      inline: this.args.outputFieldArgs.inline,
    };
  }

  <template>
    {{#let
      (component @outputComponent value=this.value options=this.outputOptions)
      as |BoundOutputComponent|
    }}
      {{#if @outputFieldArgs.inline}}
        <BoundOutputComponent
          {{! @glint-ignore ...attributes type is wrong }}
          class='inline'
          ...attributes
        />
      {{else}}
        <this.layoutComponent
          @outputComponent={{BoundOutputComponent}}
          @label={{this.label}}
          @prefixes={{this.prefixes}}
          @suffixes={{this.suffixes}}
          {{! @glint-ignore ...attributes type is wrong }}
          ...attributes
        />
      {{/if}}
    {{/let}}
  </template>
}
