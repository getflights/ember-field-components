import type { ComponentLike } from '@glint/template';

export interface OutputFieldLayoutSignature<E extends Element = HTMLElement> {
  Args: {
    label: string | undefined;
    prefixes: string[] | undefined;
    suffixes: string[] | undefined;
    outputComponent: ComponentLike<{ Element: E }>;
  };
  Element: E;
}

export type SomeOutputFieldLayout = ComponentLike<OutputFieldLayoutSignature>;
