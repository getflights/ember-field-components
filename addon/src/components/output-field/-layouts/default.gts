import type { OutputFieldLayoutSignature } from './index.ts';
import Component from '@glimmer/component';

export default class OutputFieldLayoutDefault extends Component<OutputFieldLayoutSignature> {
  private readonly outputClass: string = 'output-field__output';

  <template>
    <div class='output-field'>
      {{#if @label}}
        <label class='output-field__label'>
          {{@label}}
        </label>
      {{/if}}

      <div class='output-field__box'>
        {{#each @prefixes as |prefix|}}
          <span class='output-field__prefix'>
            {{prefix}}
          </span>
        {{/each}}

        <@outputComponent class={{this.outputClass}} ...attributes />

        {{#each @suffixes as |suffix|}}
          <span class='output-field__suffix'>
            {{suffix}}
          </span>
        {{/each}}
      </div>
    </div>
  </template>
}
