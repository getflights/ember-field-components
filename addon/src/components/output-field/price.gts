import Component from '@glimmer/component';
import OutputPriceComponent, {
  type OutputPriceOptions,
  type OutputPriceSignature,
} from '../output/price.gts';
import OutputFieldTemplate, { type OutputFieldArguments } from './-base.gts';
import type { FieldOf, SomeModel } from '../..';
import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information';

export default class OutputFieldPriceComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputPriceOptions>;
  Element: OutputPriceSignature['Element'];
}> {
  @service declare fieldInformation: FieldInformationService;

  get currency() {
    return this.args.model['currency'] &&
      typeof this.args.model['currency'] === 'string'
      ? this.args.model['currency']
      : this.fieldInformation.defaultCurrency;
  }

  get outputFieldArgs() {
    const options = {
      ...this.args,
      outputOptions: {
        currency: this.currency,
        ...this.args.outputOptions,
      },
    };

    return options;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputPriceComponent}}
      @fieldType='price'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
