import Component from '@glimmer/component';
import type { BaseInputArguments } from './-base';
import { on } from '@ember/modifier';

export interface InputTextareaArguments extends BaseInputArguments {
  value: string | undefined;
  valueChanged?: (newValue: string) => void;
}

export interface InputTextareaSignature {
  Args: InputTextareaArguments;
  Element: HTMLTextAreaElement;
}

export default class InputTextareaComponent extends Component<InputTextareaSignature> {
  get disabled() {
    return !!this.args.disabled;
  }

  valueChanged = (ev: Event) => {
    if (this.disabled) {
      return;
    }

    if (ev.currentTarget && 'value' in ev.currentTarget) {
      const newValue = ev.currentTarget.value as string;
      this.args.valueChanged?.(newValue);
    }
  };

  <template>
    <textarea
      class='input input-textarea'
      rows='3'
      ...attributes
      value={{@value}}
      disabled={{this.disabled}}
      required={{@required}}
      {{on 'input' this.valueChanged}}
    />
  </template>
}
