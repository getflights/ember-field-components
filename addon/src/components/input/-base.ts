/* eslint-disable @typescript-eslint/no-explicit-any */
export interface BaseInputArguments {
  value?: any;
  valueChanged?: (newValue: any) => void;
  disabled?: boolean;
  required?: boolean;
}

export interface SomeInputArguments<
  IO extends Record<never, never> = Record<never, never>,
> {
  value: any | undefined;
  valueChanged: ((newValue: any) => void) | undefined;
  disabled: boolean | undefined;
  required: boolean | undefined;
  options: IO | undefined;
}
