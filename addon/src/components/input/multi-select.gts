import { isBlank } from '@ember/utils';
import not from 'ember-truth-helpers/helpers/not';
import { on } from '@ember/modifier';
import Component from '@glimmer/component';
import { isGroup } from '../../interfaces/select-option-group.ts';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import type SelectOption from '../../interfaces/select-option.ts';
import type SelectOptionGroup from '../../interfaces/select-option-group.ts';
import type { BaseInputArguments } from './-base.ts';

export interface InputMultiSelectOptions {
  selectOptions: (SelectOption | SelectOptionGroup)[];
  placeholder?: string;
}

export interface InputSelectArguments<
  Required extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value: string[] | undefined;
  valueChanged?: (
    newValue: Required extends true ? string[] : string[] | undefined,
  ) => void;
  required?: Required;
  options: InputMultiSelectOptions;
}

export interface InputSelectSignature<Required extends boolean | undefined> {
  Args: InputSelectArguments<Required>;
  Element: HTMLSelectElement;
}

export default class InputMultiSelectComponent<
  Required extends boolean | undefined,
> extends Component<InputSelectSignature<Required>> {
  @service declare intl: IntlService;

  get showNone() {
    return isBlank(this.args.value) || !this.args.required;
  }

  get noneLabel() {
    return this.args.options?.placeholder ?? this.intl.t('input.select.none');
  }

  isSelected = (value: string) => {
    return !!this.args.value?.includes(value);
  };

  get isValidValue() {
    return this.invalidValues.length === 0;
  }

  get invalidValues() {
    return (
      this.args.value?.filter((value) => {
        return !this.valueInOptions(value);
      }) ?? []
    );
  }

  valueInOptions = (value: string) => {
    return this.args.options.selectOptions.some((selectOption) => {
      if (isGroup(selectOption)) {
        return selectOption.selectOptions.some(
          (groupedOption) => groupedOption.value == value,
        );
      } else {
        return selectOption.value == value;
      }
    });
  };

  valueChanged = (ev: Event) => {
    if (this.args.disabled) {
      return;
    }

    if (ev.currentTarget && 'options' in ev.currentTarget) {
      const newValue: string[] = [];

      for (const option of (ev.currentTarget as HTMLSelectElement).options) {
        if (option.selected && !isBlank(option.value)) {
          newValue.push(option.value);
        }
      }

      this.args.valueChanged?.(newValue);
    }
  };

  formatInvalid(value?: string) {
    return `(?) ${value}`;
  }

  get size() {
    let sum = this.args.options.selectOptions.reduce((sum, opt) => {
      if (isGroup(opt)) {
        // Options + label
        sum += opt.selectOptions.length + 1;
      } else {
        sum += 1;
      }

      return sum;
    }, 0);

    if (this.showNone) {
      sum++;
    }

    sum += this.invalidValues.length;

    return sum;
  }

  <template>
    <select
      class='input input-multi-select{{unless
          this.isValidValue
          " input--invalid"
        }}'
      size={{this.size}}
      ...attributes
      {{on 'input' this.valueChanged}}
      multiple
      required={{@required}}
      disabled={{@disabled}}
    >
      {{#if this.showNone}}
        <option selected={{not @value}} value='' disabled={{@required}}>
          {{this.noneLabel}}
        </option>
      {{/if}}
      {{#each @options.selectOptions as |selectOption|}}
        {{#if (isGroup selectOption)}}
          <optgroup label={{selectOption.label}}>
            {{#each selectOption.selectOptions as |groupedSelectOption|}}
              <option
                value={{groupedSelectOption.value}}
                selected={{this.isSelected groupedSelectOption.value}}
              >{{groupedSelectOption.label}}</option>
            {{/each}}
          </optgroup>
        {{else}}
          <option
            value={{selectOption.value}}
            selected={{this.isSelected selectOption.value}}
          >{{selectOption.label}}</option>
        {{/if}}
      {{/each}}
      {{#each this.invalidValues as |invalidValue|}}
        <option
          class='input--invalid'
          value={{invalidValue}}
          selected={{true}}
          disabled={{true}}
        >
          {{this.formatInvalid invalidValue}}
        </option>
      {{/each}}
    </select>
  </template>
}
