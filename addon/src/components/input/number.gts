import Component from '@glimmer/component';
import type { BaseInputArguments } from './-base';
import { isBlank } from '@ember/utils';
import { on } from '@ember/modifier';
import { modifier } from 'ember-modifier';

export interface InputNumberOptions {
  precision?: number;
  decimals?: number;
}

export interface InputNumberArguments extends BaseInputArguments {
  value: number | undefined;
  valueChanged?: (newValue: number | undefined) => void;
  options?: InputNumberOptions;
}

export interface InputNumberSignature {
  Args: InputNumberArguments;
  Element: HTMLInputElement;
}

export default class InputNumberComponent extends Component<InputNumberSignature> {
  get disabled() {
    return !!this.args.disabled;
  }

  get isValidValue() {
    return this.args.value === undefined || typeof this.args.value === 'number';
  }

  valueChanged = (ev: Event) => {
    if (this.disabled) {
      return;
    }

    if (ev.target instanceof HTMLInputElement) {
      /**
       * The browser has a weird bug where a minus sign that the start of the input or a dot at the start or end of a string, results in an empty .value
       * This is why we need to ignore events where the input is empty and the data is just a minus or dot.
       */
      // if (
      //   ev.target.value === '' &&
      //   ['-', '.'].includes((ev as InputEvent).data!)
      // ) {
      //   return;
      // }

      const valueAsNumber = ev.target.valueAsNumber;

      let newValue = isNaN(valueAsNumber) ? undefined : valueAsNumber;
      let valueModified = false;

      // Apply maximum amount of decimals (cut off excess)
      if (newValue && this.args.options?.decimals !== undefined) {
        const [beforeSeparator, afterSeparator] = `${newValue}`.split('.');

        if (beforeSeparator && afterSeparator) {
          const correctedDecimalsValue = parseFloat(
            `${beforeSeparator}.${afterSeparator.substring(
              0,
              this.args.options.decimals,
            )}`,
          );

          if (correctedDecimalsValue !== newValue) {
            valueModified = true;
            newValue = correctedDecimalsValue;
          }
        }
      }

      this.args.valueChanged?.(newValue);

      /**
       * Ideally, I would like the input to be empty when the value is undefined.
       * But practically, this is not possible, because when a value like "12." is filled in, the input has a value of "".
       * This would clear the input whenever the user backspaces a number with 1 decimal (e.g. 1.1 would not become 1. but undefined)
       */
      // if (newValue === undefined) {
      //   ev.target.value = '';
      // }

      if (valueModified) {
        ev.target.value = newValue!.toString();
      }
    }
  };

  inputFocussed = () => {
    if (!this.isValidValue) {
      this.args.valueChanged?.(undefined);
    }
  };

  // get step() {
  //   if (this.args.options?.decimals && this.args.options.decimals > 0) {
  //     let step = '0.';

  //     for (let i = 0; i < this.args.options.decimals; i++) {
  //       step += i === this.args.options.decimals - 1 ? '1' : '0';
  //     }

  //     return step;
  //   }

  //   return undefined;
  // }

  get precisionMax() {
    if (this.args.options?.precision) {
      const decimalsLength = this.args.options?.decimals ?? 0;
      const integerLength = this.args.options.precision - decimalsLength;

      let max = integerLength > 0 ? '9'.repeat(integerLength) : '0';

      if (decimalsLength > 0) {
        max += `.${'9'.repeat(decimalsLength)}`;
      }

      return parseFloat(max);
    }

    return undefined;
  }

  inputNumberModifier = modifier((element: HTMLInputElement) => {
    const elementMin = element?.min ? parseFloat(element.min) : undefined;
    const elementMax = element?.max ? parseFloat(element.max) : undefined;

    // Set min / max args to the most strict value (set by user as arg on input vs defined by the @precision argument)
    if (this.precisionMax !== undefined) {
      const precisionMin = -1 * this.precisionMax;

      if (!elementMin || precisionMin > elementMin) {
        element.min = precisionMin.toString();
      }

      if (!elementMax || this.precisionMax < elementMax) {
        element.max = this.precisionMax.toString();
      }
    }

    // Make sure element.value and this.args.value are "the same"
    const numberValue = isBlank(element.value)
      ? undefined
      : parseFloat(element.value);

    if (numberValue !== this.args.value) {
      if (element.value !== '-') {
        element.value = this.args.value?.toString() ?? '';
      }
    }

    return () => {
      // Reset min and max to their original values
      element.min = elementMin?.toString() ?? '';
      element.max = elementMax?.toString() ?? '';
    };
  });

  <template>
    <input
      type={{if this.isValidValue 'number' 'text'}}
      class='input input-number{{unless this.isValidValue " input--invalid"}}'
      {{!-- step={{this.step}} --}}
      ...attributes
      disabled={{this.disabled}}
      required={{@required}}
      {{on 'input' this.valueChanged}}
      {{on 'focus' this.inputFocussed}}
      {{this.inputNumberModifier}}
    />
  </template>
}
