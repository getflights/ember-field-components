import Component from '@glimmer/component';
import type { ComponentLike } from '@glint/template';
import OutputFieldTextComponent from './output-field/text.gts';
import { service } from '@ember/service';
import type FieldComponentsService from '../services/field-components';
import type { FieldOf, SomeModel } from '../index.ts';
import type { OutputFieldArguments } from './output-field/-base.gts';

export interface SomeOutputFieldSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  Args: OutputFieldArguments<O, F>;
  Element: HTMLElement;
}

type OutputFieldSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> = SomeOutputFieldSignature<O, F>;

/**
 * The default OutputField.
 *
 * Looks up the options passed to the @field() decorator to decide on:
 * - Which output field should be rendered? (text, number...)
 * - Which output options should be passed on?
 * - ...
 * Most of these options can be overwritten arguments.
 *
 * When there are no field options defined, it will render an OutputField::Text with no default options.
 */
export default class OutputFieldComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<OutputFieldSignature<O, F>> {
  @service declare fieldComponents: FieldComponentsService;

  private get outputFieldComponent(): ComponentLike<
    SomeOutputFieldSignature<O, F>
  > {
    // Always fallback to a text output field: make it easy for the developer to add outputs without adding a field decorator.
    return this.fieldDecoratorOptions?.outputField ?? OutputFieldTextComponent;
  }

  /**
   * Get the options passed to the @field() decorator on the field.
   */
  private get fieldDecoratorOptions() {
    return this.fieldComponents.getFieldOptions(
      this.args.model,
      this.args.field,
    );
  }

  private get outputFieldOptions() {
    return this.fieldDecoratorOptions?.outputFieldOptions;
  }

  /**
   * Options to be passed to the output.
   * These are the options defined on the decorator and can be overwritten by the options passed by '@outputOptions' argument.
   */
  private get outputOptions() {
    return {
      // Options from the decorator
      ...this.outputFieldOptions?.outputOptions,
      // Options can be overwritten by options passed by args
      ...this.args.outputOptions,
    };
  }

  private get prefixes(): string[] {
    const prefixes = [];

    // Argument prefixes first
    if (this.args.prefix) {
      prefixes.push(
        ...(Array.isArray(this.args.prefix)
          ? this.args.prefix
          : [this.args.prefix]),
      );
    }

    // Followed by Output Field Options prefix(es)
    if (this.outputFieldOptions?.prefix) {
      prefixes.push(
        ...(Array.isArray(this.outputFieldOptions.prefix)
          ? this.outputFieldOptions.prefix
          : [this.outputFieldOptions.prefix]),
      );
    }

    return prefixes;
  }

  private get suffixes(): string[] {
    const suffixes = [];

    // Output Field Options suffix(es) first
    if (this.outputFieldOptions?.suffix) {
      suffixes.push(
        ...(Array.isArray(this.outputFieldOptions.suffix)
          ? this.outputFieldOptions.suffix
          : [this.outputFieldOptions.suffix]),
      );
    }

    // Followed by argument suffix(es)
    if (this.args.suffix) {
      suffixes.push(
        ...(Array.isArray(this.args.suffix)
          ? this.args.suffix
          : [this.args.suffix]),
      );
    }

    return suffixes;
  }

  <template>
    <this.outputFieldComponent
      @model={{@model}}
      @field={{@field}}
      @overrideValue={{@overrideValue}}
      @inline={{@inline}}
      @outputOptions={{this.outputOptions}}
      @label={{@label}}
      @prefix={{this.prefixes}}
      @suffix={{this.suffixes}}
      @showErrors={{@showErrors}}
      @layout={{@layout}}
      ...attributes
    />
  </template>
}
