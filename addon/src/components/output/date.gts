import { service } from '@ember/service';
import type FieldInformationService from '../../services/field-information';
import Component from '@glimmer/component';
import { format, isValid } from 'date-fns';
import IntlService from 'ember-intl/services/intl';
import type { BaseOutputArguments } from './-base';
import { isNone } from '@ember/utils';

export interface OutputDateOptions {
  displayTime?: boolean;
}

export interface OutputDateArguments extends BaseOutputArguments {
  value: Date | undefined;
  options?: OutputDateOptions;
}

export interface OutputDateSignature {
  Args: OutputDateArguments;
  Element: HTMLSpanElement;
}

export default class OutputDateComponent extends Component<OutputDateSignature> {
  @service declare fieldInformation: FieldInformationService;
  @service declare intl: IntlService;

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    if (!isValid(this.args.value)) {
      return typeof this.args.value === 'string'
        ? `"${this.args.value}"`
        : this.intl.t('output.date.invalid');
    }

    return format(
      this.args.value,
      this.args.options?.displayTime
        ? this.fieldInformation.dateTimeFormat
        : this.fieldInformation.dateFormat,
    );
  }

  <template>
    <span
      class='output output-date{{if (isNone this.value) " output--empty"}}'
      ...attributes
    >{{this.value}}</span>
  </template>
}
