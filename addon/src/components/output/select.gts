import Component from '@glimmer/component';
import type { BaseOutputArguments } from './-base';
import type SelectOption from '../../interfaces/select-option';
import type SelectOptionGroup from '../../interfaces/select-option-group';
import { isGroup } from '../../interfaces/select-option-group.ts';
import { isNone } from '@ember/utils';

export interface OutputSelectOptions {
  selectOptions: (SelectOption | SelectOptionGroup)[];
  placeholder?: string;
}

export interface OutputSelectArguments extends BaseOutputArguments {
  value: string | undefined;
  options: OutputSelectOptions;
}

export interface OutputSelectSignature {
  Args: OutputSelectArguments;
  Element: HTMLSpanElement;
}

export default class OutputSelectComponent extends Component<OutputSelectSignature> {
  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    const allOptions = this.args.options.selectOptions.flatMap((option) => {
      return isGroup(option) ? option.selectOptions : option;
    });
    const selectOption = allOptions.find(
      (option) => option.value === this.args.value!,
    );
    return selectOption?.label ?? `(?) ${this.args.value!}`;
  }

  <template>
    <span
      class='output output-select{{if
          (isNone this.value)
          (if @options.placeholder " output--placeholder" " output--empty")
        }}'
      ...attributes
    >
      {{if (isNone this.value) @options.placeholder this.value}}
    </span>
  </template>
}
