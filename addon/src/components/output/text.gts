import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import type { BaseOutputArguments } from './-base';
import DOMPurify from 'dompurify';
import { isNone } from '@ember/utils';

export interface OutputTextOptions {
  multiLine?: boolean;
}

export interface OutputTextArguments extends BaseOutputArguments {
  value: string | undefined;
  options?: OutputTextOptions;
}

export interface OutputTextSignature {
  Args: OutputTextArguments;
  Element: HTMLSpanElement;
}

export default class OutputTextComponent extends Component<OutputTextSignature> {
  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    if (!this.args.options?.multiLine) {
      return this.args.value;
    }

    // Replace newline with <br>
    const newlineToBr = this.args.value.replace(/\n/g, '<br>');
    // ALWAYS SANITIZE BEFORE USING HTMLSAFE
    const sanitizedString = DOMPurify.sanitize(newlineToBr);
    return htmlSafe(sanitizedString);
  }

  <template>
    <span
      class='output output-text{{if (isNone this.value) " output--empty"}}'
      ...attributes
    >
      {{this.value}}
    </span>
  </template>
}
