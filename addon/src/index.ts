// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type SomeModel = Record<string, any>;
export type FieldOf<O> = keyof O & string;
