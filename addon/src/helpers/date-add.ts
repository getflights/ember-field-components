import { isBlank } from '@ember/utils';
import { type Duration } from 'date-fns';
import { add } from 'date-fns';

export default function dateAdd(
  value: Date | undefined | null,
  duration: Duration,
) {
  if (!value || isBlank(value)) {
    return undefined;
  }

  return add(value, duration);
}
