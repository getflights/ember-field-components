import { registerLibrary } from '../version.ts';

export function initialize(/* container, application */) {
  registerLibrary();
}

export default {
  name: 'field-components',
  initialize,
};
