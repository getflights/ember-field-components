import EmberDataModel from '@ember-data/model';
import Service, { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { tracked } from 'tracked-built-ins';
import type { FieldOf, SomeModel } from '../index.ts';

export default class FieldInformationService extends Service {
  @service declare intl: IntlService;

  //#region DATETIME
  /**
   * Default date format (unicode tokens)
   * https://unicode.org/reports/tr35/tr35-dates.html
   */
  @tracked dateFormat = 'yyyy-MM-dd';
  /**
   * Default datetime format (unicode tokens)
   * https://unicode.org/reports/tr35/tr35-dates.html
   */
  @tracked dateTimeFormat = 'yyyy-MM-dd HH:mm:ss';
  /**
   * Default time format (unicode tokens)
   * https://unicode.org/reports/tr35/tr35-dates.html
   */
  @tracked timeFormat = 'HH:mm:ss';
  //#endregion

  //#region CURRENCY
  @tracked defaultCurrency = 'EUR';
  @tracked currencyDisplay: 'symbol' | 'code' | 'name' | 'narrowSymbol' =
    'symbol';
  availableCurrencies: string[] = ['EUR', 'USD', 'GBP'];
  //#endregion

  /**
   * Returns the translated value of a field label. If nothing is found, the field will be capitalized
   * @param modelName The name of the model
   * @param field The field
   */
  getTranslatedFieldLabel(modelOrModelName: string, field: string): string;
  getTranslatedFieldLabel<O extends SomeModel, F extends FieldOf<O>>(
    modelOrModelName: O,
    field: F,
  ): string;
  getTranslatedFieldLabel<O extends SomeModel, F extends FieldOf<O>>(
    modelOrModelName: O | string,
    field: F | string,
  ): string {
    let modelName: string | undefined;

    if (typeof modelOrModelName === 'string') {
      modelName = modelOrModelName;
    } else if (modelOrModelName instanceof EmberDataModel) {
      // @ts-expect-error constructor.modelName is not typed
      modelName = modelOrModelName.constructor.modelName;
    }

    if (modelName) {
      const modelIntlKey = `ember-field-components.${modelName}.fields.${field}`;
      if (this.intl.exists(modelIntlKey)) {
        return this.intl.t(modelIntlKey);
      }
    }

    const globalIntlKey = `ember-field-components.global.fields.${field}`;
    if (this.intl.exists(globalIntlKey)) {
      return this.intl.t(globalIntlKey);
    }

    // Capitalize and add spaces every time a new uppercase letter appears.
    let fieldLabel = field.replace(/([A-Z])/g, ' $1');
    fieldLabel = fieldLabel.charAt(0).toUpperCase() + fieldLabel.slice(1);
    return fieldLabel;
  }

  /**
   * Returns the translated value of a field helptext. If nothing is found, the field will be capitalized
   * @param modelName The name of the model
   * @param field The field
   */
  getTranslatedFieldHelptext(
    modelOrModelName: string,
    field: string,
  ): string | undefined;
  getTranslatedFieldHelptext<O extends SomeModel, F extends FieldOf<O>>(
    modelOrModelName: O,
    field: F,
  ): string | undefined;
  getTranslatedFieldHelptext<O extends SomeModel, F extends FieldOf<O>>(
    modelOrModelName: O | string,
    field: F | string,
  ): string | undefined {
    let modelName: string | undefined;

    if (typeof modelOrModelName === 'string') {
      modelName = modelOrModelName;
    } else if (modelOrModelName instanceof EmberDataModel) {
      // @ts-expect-error constructor.modelName is not typed
      modelName = modelOrModelName.constructor.modelName;
    }

    if (modelName) {
      const modelIntlKey = `ember-field-components.${modelName}.helptexts.${field}`;
      if (this.intl.exists(modelIntlKey)) {
        return this.intl.t(modelIntlKey);
      }
    }

    const globalIntlKey = `ember-field-components.global.helptexts.${field}`;
    if (this.intl.exists(globalIntlKey)) {
      return this.intl.t(globalIntlKey);
    }

    return undefined;
  }
}
