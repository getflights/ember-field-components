import Service from '@ember/service';
import { tracked } from 'tracked-built-ins';
import type { SomeInputFieldLayout } from '../components/input-field/-layouts';
import InputFieldLayoutDefault from '../components/input-field/-layouts/default.gts';
import type { SomeOutputFieldLayout } from '../components/output-field/-layouts/index.ts';
import OutputFieldLayoutDefault from '../components/output-field/-layouts/default.gts';
import {
  METADATA_KEY,
  type FieldDecoratorOptions,
} from '../decorators/field.ts';
import type { FieldOf, SomeModel } from '../index.ts';

export default class FieldComponentsService extends Service {
  getFieldOptions = <O extends SomeModel, F extends FieldOf<O>>(
    target: O,
    field: F,
  ): FieldDecoratorOptions<O, F> | undefined => {
    const fields = field.split('.');

    let currentValue = target;
    for (let i = 0; i < fields.length; i++) {
      const currentField = fields[i]!;

      if (currentValue) {
        if (i === fields.length - 1) {
          return Reflect.getMetadata(
            METADATA_KEY,
            currentValue,
            currentField,
          ) as FieldDecoratorOptions<O, F> | undefined;
        } else {
          currentValue = target[currentField];
        }
      }
    }
  };

  //#region LAYOUTS
  @tracked private inputFieldLayoutMap = new Map<
    string,
    SomeInputFieldLayout
  >();

  @tracked private outputFieldLayoutMap = new Map<
    string,
    SomeOutputFieldLayout
  >();

  @tracked private defaultInputFieldLayout: SomeInputFieldLayout =
    InputFieldLayoutDefault;

  /**
   * Overwrite the default layout of all input fields.
   *
   * ```ts
   * import InputFieldBootstrapLayoutDefault from 'app-or-addon/components/input-field/-layouts/bootstrap/default.gts';
   *
   * export function initialize(applicationInstance) {
   *  const fieldService = applicationInstance.lookup('service:field-components');
   *
   *  fieldService.setDefault(InputFieldBootstrapLayoutDefault);
   * }
   * ```
   */
  setDefaultInputFieldLayout = (layout: SomeInputFieldLayout) => {
    this.defaultInputFieldLayout = layout;
  };

  @tracked private defaultOutputFieldLayout: SomeOutputFieldLayout =
    OutputFieldLayoutDefault;

  /**
   * Overwrite the default layout of all input fields.
   *
   * ```ts
   * import InputFieldBootstrapLayoutDefault from 'app-or-addon/components/input-field/-layouts/bootstrap/default.gts';
   *
   * export function initialize(applicationInstance) {
   *  const fieldService = applicationInstance.lookup('service:field-components');
   *
   *  fieldService.setDefault(InputFieldBootstrapLayoutDefault);
   * }
   * ```
   */
  setDefaultOutputFieldLayout = (layout: SomeOutputFieldLayout) => {
    this.defaultOutputFieldLayout = layout;
  };

  /**
   * Overwrite the layout for one type of input field.
   *
   * ```ts
   * import InputFieldBootstrapLayoutCheckbox from 'app-or-addon/components/input-field/-layouts/bootstrap/checkbox.gts';
   *
   * export function initialize(applicationInstance) {
   *  const fieldService = applicationInstance.lookup('service:field-components');
   *
   *  fieldService.registerInputLayout('checkbox', InputFieldBootstrapLayoutCheckbox);
   * }
   * ```
   */
  registerInputFieldLayout = (
    fieldType: string,
    layout: SomeInputFieldLayout,
  ) => {
    this.inputFieldLayoutMap.set(fieldType, layout);
  };

  /**
   * Overwrite the layout for one type of input field.
   *
   * ```ts
   * import InputFieldBootstrapLayoutCheckbox from 'app-or-addon/components/input-field/-layouts/bootstrap/checkbox.gts';
   *
   * export function initialize(applicationInstance) {
   *  const fieldService = applicationInstance.lookup('service:field-components');
   *
   *  fieldService.registerInputLayout('checkbox', InputFieldBootstrapLayoutCheckbox);
   * }
   * ```
   */
  registerOutputFieldLayout = (
    fieldType: string,
    layout: SomeOutputFieldLayout,
  ) => {
    this.outputFieldLayoutMap.set(fieldType, layout);
  };

  /**
   * Retrieve the registered layout for an input type.
   * If not found, it will return the default layout.
   */
  inputFieldLayoutFor = (fieldType: string): SomeInputFieldLayout => {
    return (
      this.inputFieldLayoutMap.get(fieldType) ?? this.defaultInputFieldLayout
    );
  };

  /**
   * Retrieve the registered layout for an input type.
   * If not found, it will return the default layout.
   */
  outputFieldLayoutFor = (fieldType: string): SomeOutputFieldLayout => {
    return (
      this.outputFieldLayoutMap.get(fieldType) ?? this.defaultOutputFieldLayout
    );
  };
}
