import type SelectOption from './select-option';

export default interface SelectOptionGroup {
  label: string;
  selectOptions: SelectOption[];
}

export function isGroup(
  optionOrGroup: SelectOption | SelectOptionGroup,
): optionOrGroup is SelectOptionGroup {
  return 'selectOptions' in optionOrGroup;
}
